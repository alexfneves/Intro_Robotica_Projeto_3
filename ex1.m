clear;

ex1_exp;

%             th    d       a         alpha      type   offset    mdh
L1(1) = Link([ 0     0       0          pi/2         0     pi/2],  'standard');
L1(2) = Link([ 0 	0       0         -pi/2         1      0],  'standard');
L1(3) = Link([ 0     0	   (R-r)         0          0    -pi/2],  'standard');
L2(1) = Link([ 0    0       0          pi/2         0     pi/2],  'standard');
L2(2) = Link([ 0 	0       0         -pi/2         1      0],  'standard');
L2(3) = Link([ 0    0	   (R-r)         0          0    -pi/2],  'standard');
L3(1) = Link([ 0    0       0          pi/2         0     pi/2],  'standard');
L3(2) = Link([ 0 	0       0         -pi/2         1      0],  'standard');
L3(3) = Link([ 0    0	    0            0          0    -pi/2],  'standard');
planar1 = SerialLink(L1, 'base', T_01, 'name', 'Planar1', 'comment', 'Alex Fernandes Neves');
planar2 = SerialLink(L2, 'base', T_02, 'tool', T_0e_2, 'name', 'Planar2', 'comment', 'Alex Fernandes Neves');
planar3 = SerialLink(L3, 'base', T_03, 'name', 'Planar3', 'comment', 'Alex Fernandes Neves');

% Plot
limites = [-1.5 1.5 -0.5 2.5 -1 1];
plot_figures = false;
save_figure = false;

if plot_figures
%     figure(1);
%     close(1);
%     planar1.plot(t0_A(1:3)', 'floorlevel', -0.3, 'nobase', 'noname', 'notiles', 'wrist', 'noshadow', 'noshading', 'delay', 0, 'trail', 'r', 'noraise', 'top', 'workspace',  limites);
%     hold on;
%     planar2.plot(t0_A(4:6)', 'floorlevel', -0.3, 'nobase', 'noname', 'notiles', 'wrist', 'noshadow', 'noshading', 'delay', 0, 'trail', 'g', 'noraise', 'top', 'workspace',  limites);
%     planar3.plot(t0_A(7:9)', 'floorlevel', -0.3, 'nobase', 'noname', 'notiles', 'wrist', 'noshadow', 'noshading', 'delay', 0, 'trail', 'b', 'noraise', 'top', 'workspace',  limites);
%     hold off;
%     if save_figure
%         export_fig(['latex/figs/ex1_theta_A'], '-pdf', '-painters', '-transparent');
%     end
%     figure(1);
%     close(1);
%     planar1.plot(t0_B(1:3)', 'floorlevel', -0.3, 'nobase', 'noname', 'notiles', 'wrist', 'noshadow', 'noshading', 'delay', 0, 'trail', 'r', 'noraise', 'top', 'workspace',  limites);
%     hold on;
%     planar2.plot(t0_B(4:6)', 'floorlevel', -0.3, 'nobase', 'noname', 'notiles', 'wrist', 'noshadow', 'noshading', 'delay', 0, 'trail', 'g', 'noraise', 'top', 'workspace',  limites);
%     planar3.plot(t0_B(7:9)', 'floorlevel', -0.3, 'nobase', 'noname', 'notiles', 'wrist', 'noshadow', 'noshading', 'delay', 0, 'trail', 'b', 'noraise', 'top', 'workspace',  limites);
%     hold off;
%     if save_figure
%         export_fig(['latex/figs/ex1_theta_B'], '-pdf', '-painters', '-transparent');
%     end
%     figure(1);
%     close(1);
%     planar1.plot(t0_C(1:3)', 'floorlevel', -0.3, 'nobase', 'noname', 'notiles', 'wrist', 'noshadow', 'noshading', 'delay', 0, 'trail', 'r', 'noraise', 'top', 'workspace',  limites);
%     hold on;
%     planar2.plot(t0_C(4:6)', 'floorlevel', -0.3, 'nobase', 'noname', 'notiles', 'wrist', 'noshadow', 'noshading', 'delay', 0, 'trail', 'g', 'noraise', 'top', 'workspace',  limites);
%     planar3.plot(t0_C(7:9)', 'floorlevel', -0.3, 'nobase', 'noname', 'notiles', 'wrist', 'noshadow', 'noshading', 'delay', 0, 'trail', 'b', 'noraise', 'top', 'workspace',  limites);
%     hold off;
%     if save_figure
%         export_fig(['latex/figs/ex1_theta_C'], '-pdf', '-painters', '-transparent');
%     end
    
end

% parâmetros
K = 1;

n_sim = 2;
st_1 = 4*pi*(1 + 1/4)+0.001;
st_2 = 4*pi*(2 + 1/4);
st_3 = 4*pi*(4 + 1/4)+0.001;
%                   1       2       3       4       5       6       7       
tab_t0 =            [t0_B   t0_B    t0_B    t0_B    t0_B    t0_B    t0_B    t0_B    t0_C    t0_C    t0_C    t0_C];
tab_sim_time =      [st_1	st_3	st_3	st_3	st_3    st_3	st_3	st_3    st_1    st_2    st_1    st_2]';
tab_max_step_size = [1e-2	1e-1	1e-2	1e-2	1e-1    1e-2	1e-2	1e-1    1e-2    1e-2    1e-2    1e-2]';
tab_K_k =           [0      0       1       0.1     0.1     0       0       0       1       0.5     0       0]';
tab_K_0 = -         [0      0       0       0       0       1       0.1     0.1     0       0       1       0.5]';


% simulations
for i = 1:12
    t0 = tab_t0(:,i);
    K_k = tab_K_k(i);
    K_0 = tab_K_0(i);
    sim_time = tab_sim_time(i);
    max_step_size = tab_max_step_size(i);
    direto = K_k ~= 0;
    nulo = K_0 ~= 0;
    
    sim('ex1_sim');
    
    if plot_figures
        
        % plot manipulators
        figure(1);
        close(1);
        planar1.plot(t0(1:3)', 'floorlevel', -0.3, 'nobase', 'noname', 'notiles', 'nowrist', 'noshadow', 'noshading', 'delay', 0, 'trail', 'r', 'noraise', 'top', 'workspace',  limites);
        hold on;
        planar2.plot(t0(4:6)', 'floorlevel', -0.3, 'nobase', 'noname', 'notiles', 'nowrist', 'noshadow', 'noshading', 'delay', 0, 'trail', 'g', 'noraise', 'top', 'workspace',  limites);
        planar3.plot(t0(7:9)', 'floorlevel', -0.3, 'nobase', 'noname', 'notiles', 'nowrist', 'noshadow', 'noshading', 'delay', 0, 'trail', 'b', 'noraise', 'top', 'workspace',  limites);
        hold off;

        stime = size(hatt.time);
        sq = size(hatt.signals.values);
        for j = 1:5:sq(1)
            planar1.animate(hatt.signals.values(j,1:3));
            planar2.animate(hatt.signals.values(j,4:6));
            planar3.animate(hatt.signals.values(j,7:9));
        end
        planar1.animate(hatt.signals.values(sq(1),1:3));
        planar2.animate(hatt.signals.values(sq(1),4:6));
        planar3.animate(hatt.signals.values(sq(1),7:9));
        if save_figure
            export_fig(['latex/figs/ex1_' int2str(i) '_manips'], '-pdf', '-painters', '-transparent');
        end
        
        % plot hatt
        plot(hatt.time, hatt.signals.values);
        xlabel('time (s)'); ylabel('$\hat{\theta}$','Interpreter','latex');
        legend({'$\hat{\theta}_1$', '$\hat{d}_2$', '$\hat{\theta}_3$', '$\hat{\theta}_4$', '$\hat{d}_5$', '$\hat{\theta}_6$', '$\hat{\theta}_7$', '$\hat{d}_8$', '$\hat{\theta}_9$'},'Interpreter','latex');
        if save_figure
            export_fig(['latex/figs/ex1_' int2str(i) '_hatt'], '-pdf', '-painters', '-transparent');
        end
        
        % plot dhatt
        plot(dhatt.time, dhatt.signals.values);
        xlabel('time (s)'); ylabel('$\dot{\hat{\theta}}$','Interpreter','latex');
        legend({'$\dot{\hat{\theta}}_1$', '$\dot{\hat{d}}_2$', '$\dot{\hat{\theta}}_3$', '$\dot{\hat{\theta}}_4$', '$\dot{\hat{d}}_5$', '$\dot{\hat{\theta}}_6$', '$\dot{\hat{\theta}}_7$', '$\dot{\hat{d}}_8$', '$\dot{\hat{\theta}}_9$'},'Interpreter','latex');
        if save_figure
            export_fig(['latex/figs/ex1_' int2str(i) '_dhatt'], '-pdf', '-painters', '-transparent');
        end
        
        % plot hate_ast
        plot(hate__ast.time, hate__ast.signals.values);
        xlabel('time (s)'); ylabel('$\hat{e}^*$','Interpreter','latex');
        legend({'$\hat{e}_1^*$', '$\hat{e}_2^*$', '$\hat{e}_3^*$'},'Interpreter','latex');
        if save_figure
            export_fig(['latex/figs/ex1_' int2str(i) '_hate__ast'], '-pdf', '-painters', '-transparent');
        end
        
        % plot hatk
        plot(hatk.time, squeeze(hatk.signals.values(:,1,:))');
        xlabel('time (s)'); ylabel('$\hat{k}$','Interpreter','latex');
        legend({'$\hat{k}_1$', '$\hat{k}_2$', '$\hat{k}_3$', '$\hat{k}_4$', '$\hat{k}_5$', '$\hat{k}_6$'},'Interpreter','latex');
        if save_figure
            export_fig(['latex/figs/ex1_' int2str(i) '_hatk'], '-pdf', '-painters', '-transparent');
        end
    end
end