function [ dq, dx, k ] = ex1Control(q, u_bar)
%EX1CONTROL Summary of this function goes here
%   Detailed explanation goes here


%%
% if sum(sum(J_1 - J_1Func(q(1), q(2), q(3), q(4), q(5), q(6), q(7), q(8), q(9)) > 1e-15)) > 0
%     a = 0
% end
% if sum(sum(J_2 - J_2Func(q(1), q(2), q(3), q(4), q(5), q(6), q(7), q(8), q(9)) > 1e-15)) > 0
%     a = 0
% end
% if sum(sum(J_3 - J_3Func(q(1), q(2), q(3), q(4), q(5), q(6), q(7), q(8), q(9)) > 1e-15)) > 0
%     a = 0
% end

%%
J_1 = J_1Func(q(1), q(2), q(3), q(4), q(5), q(6), q(7), q(8), q(9));
J_2 = J_2Func(q(1), q(2), q(3), q(4), q(5), q(6), q(7), q(8), q(9));
J_3 = J_3Func(q(1), q(2), q(3), q(4), q(5), q(6), q(7), q(8), q(9));
%dq = zeros(9,1);

% controle tradicional para 1 junta
%dq(1:3) = inv(J_1([1:2 6],:))*u_bar([1:2 6],1);
%dq(4:6) = inv(J_2([1:2 6],:))*u_bar([1:2 6],1);
%dq(7:9) = inv(J_3([1:2 6],:))*u_bar([1:2 6],1);

S = zeros(3,6);
S(1,1) = 1;
S(2,2) = 1;
S(3,6) = 1;

J = zeros(9);
J(1:3,1:3) = S*J_1;
J(4:6,4:6) = S*J_2;
J(7:9,7:9) = S*J_3;
A = [eye(3); eye(3); eye(3)];
%Atil = [eye(3) -eye(3) zeros(3); eye(3) zeros(3) -eye(3)];
%Across = [eye(3) zeros(3) zeros(3)];
Across = pinv(A);

%Jc = Atil*J;
Jc = dkFunc(q(1), q(2), q(3), q(4), q(5), q(6), q(7), q(8), q(9));
Jm = Across*J;

% Sa = zeros(9,9);
% Sa(:,2) = 1;
% Sa(:,5) = 1;
% Sa(:,8) = 1;
% Sp = ones(9,9) - Sa;
% Jca = Jc*Sa;
% Jcp = Sp*Jc;
% Jma = Sa*Jm;
% Jmp = Sp*Jm;


Jca = Jc(:, [2 5 8]);
Jma = Jm(:, [2 5 8]);
Jcp = Jc(:, [1 3 4 6 7 9]);
Jmp = Jm(:, [1 3 4 6 7 9]);

J_bar = Jma - Jmp*pinv(Jcp)*Jca;
dqa = J_bar\u_bar;
dqp = -pinv(Jcp)*Jca*dqa;
dq = [dqp(1) dqa(1) dqp(2) dqp(3) dqa(2) dqp(4) dqp(5) dqa(3) dqp(6)]';

k = kFunc(q(1), q(2), q(3), q(4), q(5), q(6), q(7), q(8), q(9));
dk = dkFunc(q(1), q(2), q(3), q(4), q(5), q(6), q(7), q(8), q(9));

K0 = -0.1;
pJm = pinv(Jm);
mi = K0*2*(k'*dk)';
dq = dq + (eye(9) - pJm*Jm)*mi;

dx = Jm*dq;

end
