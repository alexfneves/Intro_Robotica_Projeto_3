
syms t_1 d_2 t_3 t_4 d_5 t_6 t_7 d_8 t_9 r R h real;

x = [1 0 0]';
y = [0 1 0]';
z = [0 0 1]';

% parameters
cte_r = 0.5;
cte_R = 1;
cte_h = 2;
p_0e_B = [0 1 0]';
t_0e_B = -pi/4;
t_0e_B_1 = t_0e_B;
t_0e_B_2 = t_0e_B + pi;
t_0e_B_3 = t_0e_B;
p_0e_A = [0 1 0]';
t_0e_A = 0;
t_0e_A_1 = t_0e_A;
t_0e_A_2 = t_0e_A + pi;
t_0e_A_3 = t_0e_A;

%
h_1 = z;
h_2 = x;
h_3 = z;
h_4 = z;
h_5 = x;
h_6 = z;
h_7 = z;
h_8 = x;
h_9 = z;

%
R_01 = expm(hat(h_1)*t_1);
R_12 = eye(3);
R_23 = expm(hat(h_3)*t_3);
R_04 = expm(hat(h_4)*t_4);
R_45 = eye(3);
R_56 = expm(hat(h_6)*t_6);
R_6e = expm(hat(z)*pi);
R_07 = expm(hat(h_7)*t_7);
R_78 = eye(3);
R_89 = expm(hat(h_9)*t_9);

p_01 = [-R 0 0]';
p_12 = zeros(3,1);
p_23 = d_2*h_2;
p_3e = (R-r)*x;
p_04 = [R 0 0]';
p_45 = zeros(3,1);
p_56 = d_5*h_5;
p_6e = (R-r)*x;
p_07 = [0 h 0]';
p_78 = zeros(3,1);
p_89 = d_8*h_8;
p_9e = zeros(3,1);

% Restrições
p_0e_1 = p_01 + R_01*(p_12 + R_12*(p_23 + R_23*p_3e));
p_0e_2 = p_04 + R_04*(p_45 + R_45*(p_56 + R_56*p_6e));
p_0e_3 = p_07 + R_07*(p_78 + R_78*(p_89 + R_89*p_9e));
x_0e_1_ast_subed = subs([p_0e_1(1:2,:); t_1 + t_3], [r R h], [cte_r cte_R cte_h]);
matlabFunction(x_0e_1_ast_subed, 'File', 'x_0e_1_astFunc', 'Vars', {[t_1 d_2 t_3 t_4 d_5 t_6 t_7 d_8 t_9]'});

% +2*pi é para tentar botar - t_4 - t_6 - pi na mesma faixa de rad de t_1+t_3
k = [(p_0e_1 - p_0e_2); (p_0e_1 - p_0e_3); (t_1 + t_3 - t_4 - t_6 - pi + 2*pi);  (t_1 + t_3 - t_7 - t_9)];
k(6,:) = [];
k(3,:) = [];
J_c = [diff(k, 't_1') diff(k, 'd_2') diff(k, 't_3') diff(k, 't_4') diff(k, 'd_5') diff(k, 't_6') diff(k, 't_7') diff(k, 'd_8') diff(k, 't_9')];
k_subed = subs(k, [r R h], [cte_r cte_R cte_h]);
J_c_subed = subs(J_c, [r R h], [cte_r cte_R cte_h]);
matlabFunction(k_subed, 'File', 'kFunc', 'Vars', {[t_1 d_2 t_3 t_4 d_5 t_6 t_7 d_8 t_9]'});
matlabFunction(J_c_subed, 'File', 'J_cFunc', 'Vars', {[t_1 d_2 t_3 t_4 d_5 t_6 t_7 d_8 t_9]'});

% Calculando condições iniciais (genérico)
p_0e_CI = sym('p_0e_CI', [3 1], 'real');
syms t_0e_CI t_0e_CI_1 t_0e_CI_2 t_0e_CI_3 real;
p_23_CI = p_0e_CI - p_01 - rotz(t_0e_CI_1)*p_3e;
p_56_CI = p_0e_CI - p_04 - rotz(t_0e_CI_2)*p_6e;
p_89_CI = p_0e_CI - p_07 - rotz(t_0e_CI_3)*p_9e;
d_2_CI = norm(p_23_CI);
d_5_CI = norm(p_56_CI);
d_8_CI = norm(p_89_CI);
t_1_CI = atan2(p_23_CI(2), p_23_CI(1));
t_4_CI = atan2(p_56_CI(2), p_56_CI(1));
t_7_CI = atan2(p_89_CI(2), p_89_CI(1));
t_3_CI = t_0e_CI_1 - t_1_CI;
t_6_CI = t_0e_CI_2 - t_4_CI;
t_9_CI = t_0e_CI_3 - t_7_CI;

t0_A = double(subs([t_1_CI d_2_CI t_3_CI t_4_CI d_5_CI t_6_CI t_7_CI d_8_CI t_9_CI]', [r R h p_0e_CI' t_0e_CI t_0e_CI_1 t_0e_CI_2 t_0e_CI_3], [cte_r cte_R cte_h p_0e_A' t_0e_A t_0e_A_1 t_0e_A_2 t_0e_A_3]));
t0_B = double(subs([t_1_CI d_2_CI t_3_CI t_4_CI d_5_CI t_6_CI t_7_CI d_8_CI t_9_CI]', [r R h p_0e_CI' t_0e_CI t_0e_CI_1 t_0e_CI_2 t_0e_CI_3], [cte_r cte_R cte_h p_0e_B' t_0e_B t_0e_B_1 t_0e_B_2 t_0e_B_3]));
t0_C = t0_B;
t0_C([1 3 4 6 7 9], 1) = t0_C([1 3 4 6 7 9], 1) + 0.4;

% S
S = zeros(3,6);
S(1,1) = 1;
S(2,2) = 1;
S(3,6) = 1;
S_a = zeros(9,3);
S_a(2,1) = 1;
S_a(5,2) = 1;
S_a(8,3) = 1;
S_p = zeros(9,6);
S_p(1,1) = 1;
S_p(3,2) = 1;
S_p(4,3) = 1;
S_p(6,4) = 1;
S_p(7,5) = 1;
S_p(9,6) = 1;


% Jacobianos
R_02 = R_01*R_12;
R_03 = R_02*R_23;
R_05 = R_04*R_45;
R_06 = R_05*R_56;
R_08 = R_07*R_78;
R_09 = R_08*R_89;
J_e_1 = [cross(h_1, R_01*p_12 + R_02*p_23 + R_03*p_3e) R_01*h_2 cross(R_02*h_3, R_03*p_3e); h_1 zeros(3,1) R_02*h_3];
J_e_2 = [cross(h_4, R_04*p_45 + R_05*p_56 + R_06*p_6e) R_04*h_5 cross(R_05*h_6, R_06*p_6e); h_4 zeros(3,1) R_05*h_6];
J_e_3 = [cross(h_7, R_07*p_78 + R_08*p_89 + R_09*p_9e) R_07*h_8 cross(R_08*h_9, R_09*p_9e); h_7 zeros(3,1) R_08*h_9];
J_e_1_subed = subs(J_e_1, [r R h], [cte_r cte_R cte_h]);
J_e_2_subed = subs(J_e_2, [r R h], [cte_r cte_R cte_h]);
J_e_3_subed = subs(J_e_3, [r R h], [cte_r cte_R cte_h]);
% matlabFunction(J_e_1_subed, 'File', 'J_1Func', 'Vars', {t_1 d_2 t_3 t_4 d_5 t_6 t_7 d_8 t_9});
% matlabFunction(J_e_2_subed, 'File', 'J_2Func', 'Vars', {t_1 d_2 t_3 t_4 d_5 t_6 t_7 d_8 t_9});
% matlabFunction(J_e_3_subed, 'File', 'J_3Func', 'Vars', {t_1 d_2 t_3 t_4 d_5 t_6 t_7 d_8 t_9});

A = [eye(3); eye(3); eye(3)];
A__dag = pinv(A);
J__ast = sym(zeros(9));
J__ast(1:3,1:3) = S*J_e_1_subed;
J__ast(4:6,4:6) = S*J_e_2_subed;
J__ast(7:9,7:9) = S*J_e_3_subed;
J_e_Sast = A__dag*J__ast;
J_e_Saast = J_e_Sast*S_a;
J_e_Spast = J_e_Sast*S_p;

J_c_a = J_c_subed*S_a;
J_c_p = J_c_subed*S_p;

barJ_e_Saast = simplify(J_e_Saast - J_e_Spast*inv(J_c_p)*J_c_a);

matlabFunction(J_e_Sast, 'File', 'J_e_SastFunc', 'Vars', {[t_1 d_2 t_3 t_4 d_5 t_6 t_7 d_8 t_9]'});
matlabFunction(barJ_e_Saast, 'File', 'barJ_e_SaastFunc', 'Vars', {[t_1 d_2 t_3 t_4 d_5 t_6 t_7 d_8 t_9]'});

% Trajetória
syms t real;
x_d_ast = [0.5*cos(0.5*t) 1 + 0.5*sin(0.5*t) 0]';
%dx_d_ast = [-0.5^2*sin(0.5*t) 0.5^2*cos(0.5*t) 0]';
dx_d_ast = diff(x_d_ast, t);
matlabFunction(x_d_ast, 'File', 'x_d_astFunc');
matlabFunction(dx_d_ast, 'File', 'dx_d_astFunc');


% Retirando simbólicos
p_01 = double(subs(p_01, [r R h], [cte_r cte_R cte_h]));
p_04 = double(subs(p_04, [r R h], [cte_r cte_R cte_h]));
p_07 = double(subs(p_07, [r R h], [cte_r cte_R cte_h]));
r = cte_r;
R = cte_R;
h = cte_h;

% Para o DH
T_01 = [eye(3) p_01; [0 0 0 1]];
T_02 = [eye(3) p_04; [0 0 0 1]];
T_0e_2 = [rotz(pi) zeros(3,1); [0 0 0 1]];
T_03 = [eye(3) p_07; [0 0 0 1]];
