
A = [eye(3); eye(3); eye(3)];
Across = pinv(A);
Atil = zeros(6,9);
% J_1
Atil(1, 1) = 1;
Atil(2, 2) = 1;
Atil(3, 1) = 1;
Atil(4, 2) = 1;
Atil(5:6, 3) = 1;
% J_2
Atil(1, 4) = -1;
Atil(2, 5) = -1;
Atil(5, 6) = -1;
% J_3
Atil(3, 7) = -1;
Atil(4, 8) = -1;
Atil(6, 9) = -1;

Jm = Across*J;
Jc = Atil*J;
er = simplify(Jc - dk);


S_a = zeros(9, 3);
S_a(2, 1) = 1;
S_a(5, 2) = 1;
S_a(8, 3) = 1;
S_p = zeros(9, 6);
S_p(1, 1) = 1;
S_p(3, 2) = 1;
S_p(4, 3) = 1;
S_p(6, 4) = 1;
S_p(7, 5) = 1;
S_p(9, 6) = 1;
Jma = Jm*S_a;
Jmp = Jm*S_p;
Jca = Jc*S_a;
Jcp = Jc*S_p;


